package com.example.threeballsminigame.util.navigator

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.threeballsminigame.ui.screens.MenuScreen
import com.example.threeballsminigame.ui.screens.Screens
import com.example.threeballsminigame.ui.screens.gameScreen.GameScreen
import kotlin.system.exitProcess

@Composable
fun NavigationComponent(navController: NavHostController, fixateScreen:  () -> Unit) {
    NavHost(
        navController = navController,
        startDestination = Screens.MENU_SCREEN.label
    ) {
        composable( Screens.MENU_SCREEN.label ) {
            BackHandler(true) { exitProcess(0) }
            MenuScreen()
        }
        composable( Screens.GAME_SCREEN.label ) {
            BackHandler(true) { Navigator.navigateTo(Screens.MENU_SCREEN) }
            GameScreen()
        }
    }
    val currentScreen by Navigator.navigationFlow.collectAsState(initial = Screens.MENU_SCREEN)
    if(currentScreen != Screens.MENU_SCREEN) fixateScreen()
    navController.navigate(currentScreen.label) {
        launchSingleTop = true
    }
    navController.enableOnBackPressed(true)
}