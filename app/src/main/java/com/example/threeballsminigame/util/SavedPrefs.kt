package com.example.threeballsminigame.util

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.UUID

class SavedPrefs {
    companion object {
        fun getId(context: Context) : String {
            val shP = context.getSharedPreferences("ThreeBallsMiniGame", Context.MODE_PRIVATE)
            var id = shP.getString("id", "default") ?: "default"
            if(id == "default") {
                id = UUID.randomUUID().toString()
                shP.edit().putString("id", id).apply()
            }
            return id
        }

        fun saveRecord(context: Context, record: Int) {
            val shP = context.getSharedPreferences("ThreeBallsMiniGame", Context.MODE_PRIVATE)

            val type = object: TypeToken<List<Int>>() {}.type
            val g = Gson()
            val list = g.fromJson<List<Int>>(shP.getString("records", "[]") ?: "[]", type)
            val newList = list.plus(record).sortedDescending()

            shP.edit().putString("records", g.toJson(newList)).apply()
        }

        fun getRecords(context: Context): List<Int> = Gson()
            .fromJson(context.getSharedPreferences("ThreeBallsMiniGame", Context.MODE_PRIVATE).getString("records", "[]") ?: "[]", object: TypeToken<List<Int>>() {}.type)
    }
}