package com.example.threeballsminigame.data

import androidx.compose.ui.graphics.Color

enum class Colors(val color: Color) {
    YELLOW(Color.Yellow),
    RED(Color.Red),
    BLUE(Color.Blue)
}