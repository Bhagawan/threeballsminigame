package com.example.threeballsminigame.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.threeballsminigame.R
import com.example.threeballsminigame.ui.composables.MenuButton
import com.example.threeballsminigame.ui.theme.Blue_light
import com.example.threeballsminigame.util.navigator.Navigator

@Preview
@Composable
fun MenuScreen() {
    Column(modifier = Modifier
        .fillMaxSize()
        .background(Blue_light), horizontalAlignment = Alignment.CenterHorizontally) {
        Box(modifier = Modifier.weight(1.0f).fillMaxWidth(), contentAlignment = Alignment.Center) {
            MenuButton(modifier = Modifier.fillMaxWidth(0.5f), text = stringResource(id = R.string.btn_play)) {
                Navigator.navigateTo(Screens.GAME_SCREEN)
            }
        }
    }
}