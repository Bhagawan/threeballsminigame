package com.example.threeballsminigame.ui.screens.gameScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.threeballsminigame.ui.screens.Screens
import com.example.threeballsminigame.util.navigator.Navigator
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class GameViewModel: ViewModel() {
    private val _pointsAmount = MutableStateFlow(0)
    val pointsAmount = _pointsAmount.asStateFlow()

    private val _winPopup = MutableStateFlow(false)
    val winPopup = _winPopup.asStateFlow()

    private val _restart = MutableSharedFlow<Boolean>()
    val restart = _restart.asSharedFlow()


    fun changeScore(newAmount: Int) {
        _pointsAmount.tryEmit(newAmount)
    }

    fun showWinPopup() {
        _winPopup.tryEmit(true)
    }

    fun restart() {
        viewModelScope.launch {
            _winPopup.emit(false)
            _restart.emit(true)
        }
    }

    fun back() {
        Navigator.navigateTo(Screens.MENU_SCREEN)
    }
}