package com.example.threeballsminigame.ui.screens.gameScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.threeballsminigame.ui.composables.WinPopup
import com.example.threeballsminigame.ui.theme.Blue_light
import com.example.threeballsminigame.ui.theme.Yellow_light
import com.example.threeballsminigame.ui.views.GameView
import com.example.threeballsminigame.util.SavedPrefs
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun GameScreen() {
    var init  by rememberSaveable { mutableStateOf(true) }
    val context = LocalContext.current
    val viewModel = viewModel(GameViewModel::class.java)

    val score by viewModel.pointsAmount.collectAsState()
    val winPopup by viewModel.winPopup.collectAsState()

    Box(modifier = Modifier
        .fillMaxSize()
        .background(Blue_light)) {
        AndroidView(factory = { GameView(it) },
            modifier = Modifier.fillMaxSize(),
            update = { view ->
                if (init) {
                    view.setInterface(object: GameView.GameInterface {
                        override fun win() {
                            SavedPrefs.saveRecord(context, score)
                            viewModel.showWinPopup()
                        }

                        override fun setPoints(amount: Int) = viewModel.changeScore(amount)
                    })
                    viewModel.restart.onEach { if(it) view.restart() }.launchIn(viewModel.viewModelScope)
                    init = false
                }
            })
        Text(text = score.toString(), fontSize = 20.sp, fontWeight = FontWeight.Bold, color = Color.Black, textAlign = TextAlign.Center,
            modifier = Modifier
                .align(Alignment.TopCenter)
                .background(
                    Yellow_light,
                    RoundedCornerShape(bottomEnd = 10.0f, bottomStart = 10.0f)
                )
                .padding(5.dp))
    }

    if(winPopup) WinPopup(points = score,
        onRestart = { viewModel.restart() },
        onExit = { viewModel.back() })
}