package com.example.threeballsminigame.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun MenuButton(modifier: Modifier = Modifier, text: String, onClick: () -> Unit) {
    BoxWithConstraints(modifier = modifier.clipToBounds()
        .clickable { onClick() },
        contentAlignment = Alignment.Center) {
        val brush = Brush.linearGradient(
            0.0f to Color(128, 255, 1, 255),
            0.1f to Color(255, 247, 194, 255),
            0.3f to Color(53, 155, 9, 255),
            0.9f to Color(71, 204, 63, 255),
            1.0f to Color(25, 88, 3, 255),
            start = Offset(maxWidth.value / 2.0f, 0.0f),
            end = Offset(maxWidth.value / 2.0f, maxHeight.value)
        )
        Text(text, textAlign = TextAlign.Center, fontWeight =  FontWeight.Bold, fontSize = 40.sp, color = Color.Black,
            modifier = Modifier
                .background(brush, RoundedCornerShape(10.dp))
                .padding(vertical = 10.dp, horizontal = 50.dp))
    }
}