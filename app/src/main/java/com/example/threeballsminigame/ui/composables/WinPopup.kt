package com.example.threeballsminigame.ui.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.threeballsminigame.R
import com.example.threeballsminigame.ui.theme.Green
import com.example.threeballsminigame.ui.theme.Red
import com.example.threeballsminigame.ui.theme.Transparent_black
import com.example.threeballsminigame.ui.theme.Yellow_light

@Composable
fun WinPopup(points: Int, onRestart: () -> Unit, onExit: () -> Unit) {

    AnimatedVisibility(visible = true,
        enter = fadeIn(),
        exit = fadeOut()
    ) {
        Box(modifier = Modifier
            .fillMaxSize()
            .background(Transparent_black), contentAlignment = Alignment.Center) {

            Box(modifier = Modifier
                .fillMaxSize(0.5f)
                .background(Yellow_light, RoundedCornerShape(20.0f))
                .padding(10.dp), contentAlignment = Alignment.Center) {
                Column(horizontalAlignment = Alignment.CenterHorizontally, modifier = Modifier.fillMaxSize()) {
                    Text(text = stringResource(id = R.string.header_win),
                        fontSize = 40.sp,
                        fontWeight = FontWeight.Bold,
                        color = Color.Black,
                        textAlign = TextAlign.Center)
                    Spacer(modifier = Modifier.weight(1.0f, true))
                    Text(text = stringResource(id = R.string.win_points_amount),
                        fontSize = 20.sp,
                        fontWeight = FontWeight.Bold,
                        color = Color.Black,
                        textAlign = TextAlign.Center)
                    Text(text = points.toString(),
                        fontSize = 40.sp,
                        fontWeight = FontWeight.ExtraLight,
                        color = Color.Black,
                        textAlign = TextAlign.Center)
                    Spacer(modifier = Modifier.weight(1.0f, true))
                    Row(modifier = Modifier.fillMaxWidth()) {
                        Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                            Image(painter = painterResource(id = R.drawable.baseline_arrow_back_24),
                                contentDescription = null,
                                contentScale = ContentScale.Inside,
                                modifier = Modifier.background(Red, RoundedCornerShape(10.0f))
                                    .padding(10.dp)
                                    .clipToBounds()
                                    .clickable {
                                        onExit()
                                    })
                        }
                        Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                            Image(painter = painterResource(id = R.drawable.baseline_replay_24),
                                contentDescription = null,
                                contentScale = ContentScale.Inside,
                                modifier = Modifier.background(Green, RoundedCornerShape(10.0f))
                                    .padding(10.dp)
                                    .clipToBounds()
                                    .clickable {
                                        onRestart()
                                    })
                        }
                    }
                }
            }
        }
    }
}