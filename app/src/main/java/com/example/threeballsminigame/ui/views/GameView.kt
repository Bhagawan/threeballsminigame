package com.example.threeballsminigame.ui.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.Rect
import android.view.MotionEvent
import android.view.View
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import com.example.threeballsminigame.R
import com.example.threeballsminigame.data.Colors
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Float.max
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue
import kotlin.math.pow
import kotlin.math.sqrt

class GameView(context: Context): View(context){
    private val ballBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.ball)
    private val filters :Map<Color, PorterDuffColorFilter> = Colors.values().associate { it.color to PorterDuffColorFilter(it.color.toArgb(), PorterDuff.Mode.MULTIPLY) }

    private var mWidth = 0.0f
    private var mHeight = 0.0f
    private var ballRadius = 10.0f
    private var ballSpeed = 10.0f

    private val balls = ArrayList<Ball>()
    private var activeBall = Ball(0.0f,0.0f, Colors.values().random())
    private var ballDX = 0.0f
    private var ballDY = 0.0f

    private var arrowVisible = false
    private var arrowX = 1.0f
    private var arrowY = 1.0f

    private var mInterface: GameInterface? = null

    private var points = 0
        set(value) {
            field = value
            mInterface?.setPoints(value)
        }

    private var state = STATE_AIM

    companion object {
        const val STATE_PAUSE = 0
        const val STATE_AIM = 1
        const val STATE_ANIMATION = 2
    }

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w.toFloat() - paddingStart - paddingEnd
            mHeight = h.toFloat() - paddingTop - paddingBottom
            ballRadius = mWidth / 10.5f
            ballSpeed = mHeight / 60.0f
            createLevel()
            activeBall = createBall()
            arrowX = mWidth / 2
            arrowY = mHeight
        }
    }

    override fun onDraw(canvas: Canvas) {
        if(state == STATE_ANIMATION) updateBall()
        drawBalls(canvas)
        if(arrowVisible) drawArrow(canvas)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(state) {
                    STATE_AIM -> {
                        arrowVisible = true
                        aimAt(event.x, event.y)
                    }
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when(state) {
                    STATE_AIM -> {
                        aimAt(event.x, event.y)
                    }
                }
                return true
            }

            MotionEvent.ACTION_UP -> {
                when(state) {
                    STATE_AIM -> {
                        aimAt(event.x, event.y)
                        throwBall()
                    }
                }
                return true
            }
        }
        return false
    }

    //// Public

    fun setInterface(i: GameInterface) {
        mInterface = i
    }

    fun restart() {
        createLevel()
        activeBall = createBall()
        points = 0
        state = STATE_AIM
    }

    /// Private

    private fun drawArrow(c: Canvas) {
        val aX = arrowX - mWidth  / 2.0f
        val aY = arrowY - mHeight
        val aLen = sqrt(aX.pow(2) + aY.pow(2))
        val dX = aX / aLen * 10.0f
        val dY = aY / aLen * 10.0f

        var x = mWidth / 2.0f
        var y = mHeight

        val p = Paint()
        p.strokeWidth = 2.0f
        p.color = Color.White.toArgb()
        while((x - arrowX).absoluteValue >= (dX * 3).absoluteValue) {
            c.drawLine(x,y, x + dX, y + dY, p)
            x += dX * 2
            y += dY * 2
        }
    }

    private fun drawBalls(c: Canvas) {
        val p = Paint()
        for(ball in balls) {
            p.colorFilter = filters[ball.color.color]
            c.drawBitmap(ballBitmap, null, Rect((ball.x - ballRadius).toInt(),
                (ball.y - ballRadius).toInt(), (ball.x + ballRadius).toInt(), (ball.y + ballRadius).toInt()
            ),p)
        }

        p.colorFilter = filters[activeBall.color.color]
        c.drawBitmap(ballBitmap, null, Rect((activeBall.x - ballRadius).toInt(),
            (activeBall.y - ballRadius).toInt(), (activeBall.x + ballRadius).toInt(), (activeBall.y + ballRadius).toInt()
        ),p)
    }

    private fun aimAt(x: Float, y: Float) {
        val targetY = y.coerceAtMost(mHeight * 0.9f)
        val aX = x - mWidth  / 2.0f
        val aY = targetY - mHeight
        val aLen = sqrt(aX.pow(2) + aY.pow(2))
        if(aLen > mWidth / 2.0f) {
            arrowX = mWidth / 2 + aX / aLen * (mWidth / 2)
            arrowY = mHeight + aY / aLen * (mWidth / 2)
        } else {
            arrowX = x
            arrowY = targetY
        }
    }

    private fun updateBall() {
        if(ballDX != 0.0f || ballDY != 0.0f) {
            val targetX: Float
            val targetY: Float

            if(activeBall.x + ballDX <= ballRadius) {
                targetY = (ballRadius - activeBall.x) / ballDX * ballDY + activeBall.y
                targetX = ballRadius
                ballDX *= -1.0f
            } else if(activeBall.x + ballDX >= mWidth - ballRadius) {
                targetY = (mWidth - ballRadius - activeBall.x) / ballDX * ballDY + activeBall.y
                targetX = mWidth - ballRadius
                ballDX *= -1.0f
            } else {
                targetX = activeBall.x + ballDX
                targetY = activeBall.y + ballDY
            }
            val nearBalls = getNearBalls(targetX, targetY, max(ballDX, ballDY))

            if(nearBalls.isEmpty()) {
                if(targetY < ballRadius) {
                    activeBall.x = targetX
                    activeBall.y = ballRadius
                    balls.add(activeBall)
                    activeBall = createBall()
                    ballDX = 0.0f
                    ballDY = 0.0f
                    checkGroupForLast()
                } else {
                    activeBall.x = targetX
                    activeBall.y = targetY
                }
            } else if(nearBalls.size == 1) {
                val x = nearBalls[0].x - activeBall.x
                val y = nearBalls[0].y - activeBall.y
                val l = sqrt(x.pow(2) + y.pow(2))
                activeBall.x += x / l * (l - ballRadius * 2)
                activeBall.y += y / l * (l - ballRadius * 2)
                balls.add(activeBall)
                activeBall = createBall()
                ballDX = 0.0f
                ballDY = 0.0f
                checkGroupForLast()
            } else {
                val vecX = nearBalls[1].x - nearBalls[0].x
                val vecY = nearBalls[1].y - nearBalls[0].y
                val vecL = sqrt(vecX.pow(2) + vecY.pow(2))

                var vecPerX = 1.0f
                var vecPerY = -vecX / vecY
                val vecPerL = sqrt(vecPerX.pow(2) + vecPerY.pow(2))
                vecPerX /= vecPerL
                vecPerY /= vecPerL

                val h = sqrt((ballRadius * 2).pow(2) - (vecL / 2.0f).pow(2))
                val varOneX = nearBalls[0].x + vecX / 2.0f + vecPerX * h
                val varOneY = nearBalls[0].y + vecY / 2.0f + vecPerY * h

                val varTwoX = nearBalls[0].x + vecX / 2.0f + vecPerX * h
                val varTwoY = nearBalls[0].y + vecY / 2.0f + vecPerY * h
                if(sqrt((varOneX - activeBall.x).pow(2) + (varOneY - activeBall.y).pow(2)) < sqrt((varTwoX - activeBall.x).pow(2) + (varTwoY - activeBall.y).pow(2))) {
                    activeBall.x = varOneX
                    activeBall.y = varOneY
                } else {
                    activeBall.x = varTwoX
                    activeBall.y = varTwoY
                }
                balls.add(activeBall)
                activeBall = createBall()
                ballDX = 0.0f
                ballDY = 0.0f
                checkGroupForLast()
            }
        }
    }

    private fun throwBall() {
        val aX = arrowX - mWidth  / 2.0f
        val aY = arrowY - mHeight
        val aLen = sqrt(aX.pow(2) + aY.pow(2))
        ballDX = aX / aLen * ballSpeed
        ballDY = aY / aLen * ballSpeed
        state = STATE_ANIMATION
        arrowVisible = false
    }

    private fun checkGroupForLast() {
        val nearBalls = ArrayList<Ball>()
        addGroup(nearBalls, balls.last())
        if(nearBalls.size >= 3) {
            points += 3
            if(nearBalls.size > 3) points += nearBalls.size * 3
            balls.removeAll(nearBalls.toSet())
        }
        if(balls.isEmpty()) {
            state = STATE_PAUSE
            mInterface?.win()
        } else {
            state = STATE_AIM
            activeBall = createBall()
        }
    }

    private fun addGroup(group: ArrayList<Ball>, ball: Ball) {
        group.add(ball)
        for(b in getNearBalls(ball.x, ball.y, 1.0f)) if(!group.contains(b) && ball != b && b.color == ball.color) addGroup(group, b)
    }

    private fun getNearBalls(x: Float, y: Float, uncertainty: Float): List<Ball> = balls.filter { sqrt((it.x - x).pow(2) + (it.y - y).pow(2)) <= ballRadius * 2 + uncertainty }

    private fun createLevel() {
        balls.clear()
        for (row in 0..7) {
            val xOff = if(row % 2  == 1) ballRadius else 0.0f
            for(n in 0..9) {
                balls.add(Ball(ballRadius + n * (ballRadius * 2.0f) + xOff, ballRadius * (row + 1), Colors.values().random()))
            }
        }
    }

    private fun createBall(): Ball {
        val colors = ArrayList<Colors>()
        for(b in balls) if(!colors.contains(b.color)) colors.add(b.color)
        return if(colors.isEmpty()) Ball(mWidth / 2, mHeight, Colors.values().random())
        else Ball(mWidth / 2, mHeight, colors.random())
    }

    interface GameInterface {
        fun win()
        fun setPoints(amount: Int)
    }
}