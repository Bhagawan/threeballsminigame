package com.example.threeballsminigame.ui.views

import com.example.threeballsminigame.data.Colors

data class Ball(var x: Float, var y: Float, val color: Colors)
