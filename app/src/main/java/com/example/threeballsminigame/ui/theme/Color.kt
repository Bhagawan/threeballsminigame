package com.example.threeballsminigame.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)


val Blue_light = Color(0xFF486380)
val Yellow_light = Color(0xFF976708)
val Transparent_black = Color(0x80000000)
val Green = Color(0xFF335C04)
val Red = Color(0x80A20A0A)